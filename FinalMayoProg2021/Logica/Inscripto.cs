﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Inscripto
    {
        public int Numero { get; set; }
        public int DniParticipante { get; set; }
        public int NumeroTelefono { get; set; }

        public int IncrementarNumero()
        {
            return Numero += 1;
        }
        
    }
}
