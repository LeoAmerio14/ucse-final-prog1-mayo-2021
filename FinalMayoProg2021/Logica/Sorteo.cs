﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Sorteo
    {
        public int Codigo { get; set; }
        public int CodigoDePrograma { get; set; }
        public int DniGanador { get; set; }
        public string DescripcionPremio { get; set; }
        public Inscripto Inscriptos { get; set; }

        public Sorteo(int codigoPrograma, string descripcion, Inscripto inscriptos)
        {
            this.Codigo = ObtenerCodigo();
            this.CodigoDePrograma = codigoPrograma;
            this.DniGanador = 0;
            this.DescripcionPremio = descripcion;
            this.Inscriptos = inscriptos;
        }

        public int ObtenerCodigo()
        {
            return Codigo += 1;
        }
    }
}
