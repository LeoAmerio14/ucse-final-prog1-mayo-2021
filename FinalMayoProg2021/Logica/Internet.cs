﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Internet : Programa
    {
        public enum RedSocial { Instagram, Facebook, Youtube }
        public RedSocial Red { get; set; }
        public DateTime Dias { get; set; }
        public string Titulo { get; set; }
        public string NombreCM { get; set; }

        public override string ObtenerDescripcion()
        {
            return $"No te pierdas en {Red} el streaming {Titulo} llevado adelante por {NombreCM}";
        }
    }
}
