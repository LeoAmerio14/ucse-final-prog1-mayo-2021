﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Television : Programa
    {
        public string NombreConductor1 { get; set; }
        public string NombreConductor2 { get; set; }
        public string Canal { get; set; }

        public override string ObtenerDescripcion()
        {
            return $"{NombrePrograma} - Conductores: {NombreConductor1}, {NombreConductor2} - {Dia} desde las {HoraInicio} " +
                $"hasta las {HoraFinalizacion} por el canal: {Canal}";
        }
    }
}
