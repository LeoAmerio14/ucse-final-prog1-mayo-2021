﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Principal
    {
        List<Programa> Programas = new List<Programa>();
        List<Radio> Radios = new List<Radio>();
        List<Inscripto> Inscriptos = new List<Inscripto>();
        List<Sorteo> Sorteos = new List<Sorteo>();

        public List<string> ObtenerListado(DateTime DiaSemana)
        {
            string Descripcion;
            List<string> Descripciones = new List<string>();
            Programa programa = Programas.Find(x => x.FechaEmision == DiaSemana);
            if (programa != null)
            {
                foreach (Programa item in Programas)
                {
                    Descripcion = item.ObtenerDescripcion();
                    Descripciones.OrderBy(item);
                    
                }
            }

        }
        
        public void ProgrmaRadial(DateTime dia)
        {
            Radio programa = Radios.Find(x => x.Dia == Programa.DiasSemana.Martes);
            if (programa != null)
            {
                Radios.Add(programa);
            }
        }

        public string RealizarSorteo(int nroSorteo, Programa programa)
        {
            //Programa programa = new Programa();
            int Cantidad = 0;
            Random random = new Random();
            Sorteo sorteo = Sorteos.Find(x => x.Codigo == nroSorteo);

            if (sorteo == null)
            {
                foreach (Inscripto item in Inscriptos)
                {
                    Cantidad++;
                }
                int ganador = random.Next(1, Cantidad);
                Sorteo sorteo1 = Sorteos.Find(x => x.Codigo == ganador);
                
                if (sorteo != null)
                {
                    Inscripto inscripto = Inscriptos.Find(x => x.Numero == ganador);
                    sorteo1.DniGanador = inscripto.DniParticipante;
                    sorteo1.CodigoDePrograma = programa.CodigoPrograma;
                }
                return (""+sorteo+"");
            }
            return "Sorteo Finalizado.";

        }

        
    }
}
