﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public abstract class Programa
    {
        public int CodigoPrograma { get; set; }
        public string NombrePrograma { get; set; }
        public DateTime FechaEmision { get; set; }
        public string Categoria { get; set; }
        public enum DiasSemana { Lunes, Martes, Miercoles, Jueves, Viernes }
        public DiasSemana Dia { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFinalizacion { get; set; }
        
        public int AutoinrementarCodigo()
        {
            return CodigoPrograma + 1;
        }
        
        public abstract string ObtenerDescripcion();
    }
}
