﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Radio : Programa
    {
        public string Conductor { get; set; }
        public decimal Frecuencia { get; set; }

        public override string ObtenerDescripcion()
        {
            return $"Tu radio favorita conducida por {Conductor} siempre en las misma frecuencia: {Frecuencia}";
        }

        
    }
}
